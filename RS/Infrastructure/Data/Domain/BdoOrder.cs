﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Data.Domain
{
    public class BdoOrder
    {
        public virtual int Id { get; set; }

        public virtual bool Takeaway { get; set; }

        public virtual bool Success { get; set; }

        public virtual BdoCustomer Customers { get; set; }

        public virtual IList<BdoMeal> Meals { get; set; }

        public virtual DateTime Date { get; set; }
    }
}