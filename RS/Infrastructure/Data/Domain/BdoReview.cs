﻿using Newtonsoft.Json;

namespace Infrastructure.Data.Domain
{
    public class BdoReview
    {
        public virtual int Id { get; set; }
        public virtual int Rating { get; set; }

        public virtual string Review { get; set; }

        public virtual bool Active { get; set; }

        public virtual int IdSource { get; set; }

        /// <summary>
        /// review company provider (zomato, trip advisor..)
        /// </summary>
        public virtual string Source { get; set; }
    }
}
