﻿namespace Infrastructure.Data.Domain
{
    public class BdoMeal
    {
        public virtual int Id { get; set; }

        public virtual string Name { get; set; }

        public virtual int Price { get; set; }

        public virtual bool Active { get; set; }

        public virtual string Portion { get; set; }

        public virtual int Calories { get; set; }

        public virtual string Allergens { get; set; }

        /// <summary>
        /// enum {brakfast, lunch, dinner, snack, brunch, soup}
        /// </summary>
        public virtual string MealCode { get; set; }

        public virtual string Description { get; set; }

        public virtual string Image { get; set; }


    }
}