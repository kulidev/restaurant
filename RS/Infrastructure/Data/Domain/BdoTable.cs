﻿using System.Collections.Generic;

namespace Infrastructure.Data.Domain
{
    public class BdoTable
    {
        public virtual int Id { get; set; }

        public virtual int Capacity { get; set; }

        public virtual IList<BdoReservation> Tables { get; set; }
    }
}
