﻿namespace Infrastructure.Data.Domain.PairingTables
{
    public class BdoMealMenu
    {
        public virtual int Id { get; set; }

        public virtual int MenuId { get; set; }

        public virtual int MealId { get; set; }
    }
}
