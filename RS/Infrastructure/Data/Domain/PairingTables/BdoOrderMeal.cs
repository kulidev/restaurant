﻿namespace Infrastructure.Data.Domain.PairingTables
{
    public class BdoOrderMeal
    {
        public virtual int Id { get; set; }

        public virtual int OrderId { get; set; }

        public virtual int MealId{ get; set; }
    }
}