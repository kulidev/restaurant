﻿namespace Infrastructure.Data.Domain
{
    public class BdoNewsletter
    {
        public virtual int Id { get; set; }

        public virtual string Email { get; set; }

        public virtual bool Active { get; set; }
    }
}
