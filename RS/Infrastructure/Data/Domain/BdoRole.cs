﻿namespace Infrastructure.Data.Domain
{
    public class BdoRole
    {
        public virtual int Id { get; set; }

        public virtual string Code { get; set; }
    }
}
