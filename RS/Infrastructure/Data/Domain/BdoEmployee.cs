﻿using System;

namespace Infrastructure.Data.Domain
{
    public class BdoEmployee : BdoPerson
    {
        public virtual int WorkLoad { get; set; }

        public virtual DateTime EntryDate { get; set; }

        public virtual string Address { get; set; }

        public virtual int Wage { get; set; }

        public virtual string BirthNumber { get; set; }

        public virtual int Phone { get; set; }
    }
}
