﻿namespace Infrastructure.Data.Domain
{
    public class BdoRestaurant
    {
        public virtual int Id { get; set; }
        public virtual string OpenHours { get; set; }

        public virtual string Address { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Email { get; set; }
    }
}
