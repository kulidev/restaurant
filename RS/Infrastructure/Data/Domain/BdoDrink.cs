﻿namespace Infrastructure.Data.Domain
{
    public class BdoDrink
    {
        public virtual int Id { get; set; }

        public virtual bool Active { get; set; }

        public virtual string Name { get; set; }

        public virtual int Price { get; set; }

        /// <summary>
        /// enum {beer, wine}
        /// </summary>
        public virtual string DrinkCode { get; set; }

        public virtual string Portion { get; set; }
    }
}
