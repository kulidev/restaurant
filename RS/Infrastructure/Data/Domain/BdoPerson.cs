﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Domain
{
    public class BdoPerson
    {
        public virtual int Id { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Email{ get; set; }

        public virtual string Password { get; set; }

        public virtual IList<BdoRole> Roles { get; set; }

        public virtual byte[] PasswordHash { get; set; }

        public virtual byte[] PasswordSalt { get; set; }
    }
}
