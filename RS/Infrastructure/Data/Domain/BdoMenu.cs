﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Data.Domain
{
    public class BdoMenu
    {
        public virtual int Id { get; set; }

        /// <summary>
        /// enum {standart, daily, special}
        /// </summary>
        public virtual string MenuCode { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual IList<BdoMeal> Meals { get; set; }

    }
}
