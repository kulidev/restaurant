﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data.Domain
{
    public class BdoCustomer : BdoPerson
    {
        public virtual bool DailyMenu { get; set; }

        public virtual bool SpecialOffer { get; set; }

        public virtual bool BlackList { get; set; }

        public virtual IList<BdoOrder> Orders { get; set; }
    }
}
