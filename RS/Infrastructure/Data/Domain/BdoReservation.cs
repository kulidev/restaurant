﻿using System;

namespace Infrastructure.Data.Domain
{
    public class BdoReservation
    {
        public virtual int Id { get; set; }

        public virtual bool TookPlace { get; set; }

        public virtual string OnName { get; set; }

        public virtual int CountPeople { get; set; }

        public virtual int StartTime { get; set; }

        public virtual int FinishTime { get; set; }

        public virtual int Date { get; set; }

        public virtual BdoTable Table { get; set; }

        public virtual string Email { get; set; }
    }
}
