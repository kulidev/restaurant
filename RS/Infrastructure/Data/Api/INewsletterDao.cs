﻿using System.Collections.Generic;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface INewsletterDao : IDaoBase<BdoNewsletter>
    {
        IList<BdoNewsletter> GetActiveEmails();

        BdoNewsletter GetByEmail(string email);
    }
}