﻿using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IRoleDao : IDaoBase<BdoRole>
    {
    }
}
