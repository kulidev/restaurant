﻿using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IDrinkDao : IDaoBase<BdoDrink>
    {
        
    }
}