﻿using Infrastructure.Data.Domain.PairingTables;

namespace Infrastructure.Data.Api
{
    public interface IPersonRoleDao : IDaoBase<BdoPersonRole>
    {
        
    }
}