﻿using Infrastructure.Data.Domain;
using Infrastructure.Data.Domain.PairingTables;

namespace Infrastructure.Data.Api
{
    public interface IMealMenuDao : IDaoBase<BdoMealMenu>
    {
        
    }
}