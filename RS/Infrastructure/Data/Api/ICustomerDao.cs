﻿using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface ICustomerDao : IDaoBase<BdoCustomer>
    {
        
    }
}