﻿using System;
using System.Collections.Generic;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IMenuDao : IDaoBase<BdoMenu>
    {
        BdoMenu GetMenuByDate(DateTime date, string mealType = Constants.MenuTypeDaily);

        BdoMenu GetStandartMenu();

        IList<BdoMenu> GetDailyMenus(DateTime start, DateTime finish);
    }
}