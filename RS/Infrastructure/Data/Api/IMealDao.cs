﻿using System;
using System.Collections.Generic;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IMealDao : IDaoBase<BdoMeal>
    {
        IList<BdoMeal> GetMealByIds(List<int> ids);
    }
}