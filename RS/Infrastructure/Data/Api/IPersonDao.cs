﻿using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IPersonDao : IDaoBase<BdoPerson>
    {
        BdoPerson GetPersonByEmail(string email);
    }
}