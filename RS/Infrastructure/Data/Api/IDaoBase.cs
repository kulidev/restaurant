﻿using System.Collections.Generic;
using NHibernate;

namespace Infrastructure.Data.Api
{
    public interface IDaoBase<T>
    {
        void Save(T entity);

        void Update(T entity);
        
        T LoadById(int id);
        
        void Delete(T bdo);

        IEnumerable<T> GetAll();
        void FlushAndCommit();

        T Get();

    }
}
