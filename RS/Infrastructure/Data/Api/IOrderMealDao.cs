﻿using System.Collections.Generic;
using Infrastructure.Data.Domain.PairingTables;

namespace Infrastructure.Data.Api
{
    public interface IOrderMealDao : IDaoBase<BdoOrderMeal>
    {
        List<BdoOrderMeal> GetAllByOrderId(int id);


    }
}