﻿using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IRestaurantDao : IDaoBase<BdoRestaurant>
    {
        
    }
}