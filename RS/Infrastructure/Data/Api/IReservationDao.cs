﻿using System;
using System.Collections.Generic;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IReservationDao : IDaoBase<BdoReservation>
    {
        IList<BdoReservation> GetReservation(int? date);
    }
}