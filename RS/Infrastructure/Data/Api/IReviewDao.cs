﻿using System.Collections.Generic;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Api
{
    public interface IReviewDao : IDaoBase<BdoReview>
    {
        int? GetLast();

        void SaveCollection(IList<BdoReview> reviews);

        IList<BdoReview> GetBestReviews(int count);

        IList<BdoReview> GetReviewsByActive(bool active);
    }
}
