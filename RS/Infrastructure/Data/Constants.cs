﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public static class Constants
    {
        public const string Zomato = "zomato";

        public const string MenuTypeStandart = "standart";
        public const string MenuTypeDaily = "daily";
        public const string MenuTypeSpecial = "special";

        public const string Success = "success";
        public const string Fail = "fail";
    }
}
