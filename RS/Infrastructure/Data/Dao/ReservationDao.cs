﻿using System;
using System.Collections.Generic;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Dao
{
    public class ReservationDao : DaoBase<BdoReservation>, IReservationDao
    {
        public IList<BdoReservation> GetReservation(int? date)
        {
            if (date.HasValue)
            {
                var r = CurrentSession.QueryOver<BdoReservation>().Where(x => x.Date == date.Value).List();
                return r;
            }

            return CurrentSession.QueryOver<BdoReservation>().Where(x => x.Date == date.Value).List();
        }


    }
}
