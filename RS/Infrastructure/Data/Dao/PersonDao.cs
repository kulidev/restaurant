﻿using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Dao
{
    public class PersonDao : DaoBase<BdoPerson>, IPersonDao
    {
        public BdoPerson GetPersonByEmail(string email)
        {
            return CurrentSession.QueryOver<BdoPerson>().Where(x => x.Email == email).SingleOrDefault();
        }
    }
}