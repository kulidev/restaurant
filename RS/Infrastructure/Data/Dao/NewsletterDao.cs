﻿using System.Collections.Generic;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Dao
{
    public class NewsletterDao : DaoBase<BdoNewsletter>, INewsletterDao
    {
        public IList<BdoNewsletter> GetActiveEmails()
        {
            return CurrentSession.QueryOver<BdoNewsletter>().Where(x => x.Active).List();
        }

        public BdoNewsletter GetByEmail(string email)
        {
            return CurrentSession.QueryOver<BdoNewsletter>().Where(x => x.Email == email).SingleOrDefault();
        }
    }
}