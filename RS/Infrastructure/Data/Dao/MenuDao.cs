﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;

namespace Infrastructure.Data.Dao
{
    public class MenuDao : DaoBase<BdoMenu>, IMenuDao
    {
        public BdoMenu GetMenuByDate( DateTime date, string mealType = Constants.MenuTypeDaily)
        {
            return CurrentSession.QueryOver<BdoMenu>()
                .Where(x => x.Date == date.Date && x.MenuCode == mealType).List()
                .SingleOrDefault();
        }

        public BdoMenu GetStandartMenu()
        {
            //return CurrentSession.QueryOver<BdoMenu>()
            //    .Where(x => x.Code.Code == Constants.MenuTypeDaily).SingleOrDefault();

            return CurrentSession.QueryOver<BdoMenu>().List().Where(x => x.MenuCode == Constants.MenuTypeStandart)
                .FirstOrDefault();
        }

        public IList<BdoMenu> GetDailyMenus(DateTime start, DateTime finish)
        {
            return CurrentSession.QueryOver<BdoMenu>().Where(x => x.Date >= start && x.Date <= finish && x.MenuCode == Constants.MenuTypeDaily).List();
        }
    }
}