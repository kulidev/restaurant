﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Infrastructure.Data.Dao
{
    public class ReviewDao : DaoBase<BdoReview>, IReviewDao
    {
        public int? GetLast()
        {
            var res = CurrentSession.QueryOver<BdoReview>().Where(x => x.Source == "zomato").OrderBy(x => x.Id).Desc
                .Take(1).SingleOrDefault();
            return res?.IdSource;
        }

        public void SaveCollection(IList<BdoReview> reviews)
        {
            //using (ISession session = sessionFactory.openSession())
            using (ITransaction tx = CurrentSession.BeginTransaction())
            {
                for (int i = 0; i < reviews.Count; i++)
                {
                    CurrentSession.Save(reviews[i]);
                    
                    // 10, same as the ADO batch size
                    if (i % 10 == 0)
                    {
                        // flush a batch of inserts and release memory:
                        CurrentSession.Flush();
                        CurrentSession.Clear();
                    }
                }

                tx.Commit();
            }
        }

        public IList<BdoReview> GetBestReviews(int count)
        {
            return CurrentSession.QueryOver<BdoReview>().Where(x => x.Active).OrderBy(x => x.Rating).Desc.Take(count)
                .List();
        }

        public IList<BdoReview> GetReviewsByActive(bool active)
        {
            return CurrentSession.QueryOver<BdoReview>().Where(x => x.Active == active).List();
        }
    }
}
