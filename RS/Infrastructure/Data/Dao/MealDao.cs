﻿using System;
using System.Collections.Generic;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Util;

namespace Infrastructure.Data.Dao
{
    public class MealDao : DaoBase<BdoMeal>, IMealDao
    {
        public IList<BdoMeal> GetMealByIds(List<int> ids)
        {
            var query = CurrentSession.CreateQuery("select * from meal where id in (:ids)");
            query.SetParameterList("ids", ids);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(BdoMeal)));

            return query.List<BdoMeal>();

        }
    }
}