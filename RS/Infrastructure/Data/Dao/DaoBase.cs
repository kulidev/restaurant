﻿using System;
using System.Collections.Generic;
using System.IO;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain.PairingTables;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Criterion;

namespace Infrastructure.Data.Dao
{
    public abstract class DaoBase<T> : IDaoBase<T> where T : class
    {
        protected ISession CurrentSession
        {
            get { return NHibernateHelper.Session; }
        }
        public virtual void Save(T entity)
        {
            try
            {
                using (ITransaction transaction = CurrentSession.BeginTransaction())
                {
                    CurrentSession.Save(entity);
                    transaction.Commit();
                }
            }

#warning vyhodit vyjimku ze se transakce nepodarila
            finally
            {

            }

        }

        public IEnumerable<T> GetAll()
        {
            return CurrentSession.QueryOver<T>().List();
        }

        public virtual void Update(T entity)
        {

        }

        public virtual T LoadById(int id)
        {
            var res = CurrentSession.CreateCriteria<T>().Add(Restrictions.Eq("Id", id)).UniqueResult<T>();
            return res;
        }

        public virtual T LoadById(long id, LockMode lockMode)
        {
            throw new NotImplementedException();
        }

        public virtual void Delete(T entity)
        {
            string path = Path.Combine(@"E:\Projects\restaurant\RS\Infrastructure\Data", "hibernate.cfg.xml");
            ISessionFactory factory;
            var cfg = new Configuration();
            factory = cfg
                .Configure(path).BuildSessionFactory();

            var session = factory.OpenSession();
            

            session.Delete(entity);
            session.Flush();
            session.Close();
        }

        public void FlushAndCommit()
        {
            var session = CurrentSession;

            session.Flush();
            session.BeginTransaction();
        }

        public T Get()
        {
            
            var drink = CurrentSession.Get<T>(5);
            CurrentSession.Flush();
            CurrentSession.Close();
            return drink;

        }
    }
}
