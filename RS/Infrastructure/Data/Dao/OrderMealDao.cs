﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain.PairingTables;

namespace Infrastructure.Data.Dao
{
    public class OrderMealDao : DaoBase<BdoOrderMeal>, IOrderMealDao
    {
        public List<BdoOrderMeal> GetAllByOrderId(int orderId)
        {
            return CurrentSession.QueryOver<BdoOrderMeal>().Where(x => x.OrderId == orderId).List().ToList();
        }

        
    }
}