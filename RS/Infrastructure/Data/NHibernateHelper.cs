﻿using System.IO;

using NHibernate;
using NHibernate.Cfg;

//namespace Infrastructure.Data
//{
//    public static class NHibernateHelper
//    {

//        public static ISessionFactory _factory;

//        public static ISessionFactory SessionFactory
//        {
//            get
//            {
//                if (_factory == null)
//                {

//#warning TODO predelat na relativni cestu
//                    string path = Path.Combine(@"E:\Projects\restaurant\RS\Infrastructure\Data", "hibernate.cfg.xml");
//                    var cfg = new Configuration();
//                    _factory = cfg
//                        .Configure(path).BuildSessionFactory();
//                }

//                return _factory;
//            }
//        }

//    }
//}




namespace Infrastructure.Data
{
    public static class NHibernateHelper
    {

        public static ISessionFactory _factory;

        public static ISession Session
        {
            get
            {
                if (_factory == null)
                {
                    string currentDirectory = Directory.GetCurrentDirectory();
                    string path = Path.Combine(currentDirectory, @"..\Infrastructure\Data", "hibernate.cfg.xml");
                    var cfg = new Configuration();
                    _factory = cfg
                        .Configure(path).BuildSessionFactory();
                }

                return _factory.OpenSession();
            }
        }

    }
}


