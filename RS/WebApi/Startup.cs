using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Services;
using Infrastructure.Data;
using Infrastructure.Data.Api;
using Infrastructure.Data.Dao;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using WebApi.Filters;
using WebApi.Jobs;
using WebApi.Middlewares;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NHibernate;

//using WebApi.pokusy;

//using Microsoft.AspNetCore.Mvc.NewtonsoftJson

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region dependencies
            
            //dao
            services.AddSingleton<IRoleDao, RoleDao>();
            services.AddSingleton<IReviewDao, ReviewDao>();
            services.AddSingleton<INewsletterDao, NewsletterDao>();
            services.AddSingleton<ITableDao, TableDao>();
            services.AddSingleton<IReservationDao, ReservationDao>();
            services.AddSingleton<IDrinkDao, DrinkDao>();
            services.AddSingleton<IPersonDao, PersonDao>();
            services.AddSingleton<ICustomerDao, CustomerDao>();
            services.AddSingleton<IEmployeeDao, EmployeeDao>();
            services.AddSingleton<IMenuDao, MenuDao>();
            services.AddSingleton<IMealDao, MealDao>();
            services.AddSingleton<IOrderDao, OrderDao>();
            services.AddSingleton<IRestaurantDao, RestaurantDao>();
            services.AddSingleton<IPersonRoleDao, PersonRoleDao>();
            services.AddSingleton<IOrderMealDao, OrderMealDao>();
            services.AddSingleton<IMealMenuDao, MealMenuDao>();

            //core
            services.AddSingleton<ICustomerCoreService, CustomerCoreService>();
            services.AddSingleton<IJobCoreServices, JobCoreServices>();
            services.AddSingleton<IEmailService, EmailService>();
            services.AddSingleton<IManagerCoreService, ManagerCoreService>();
            services.AddSingleton<IUserCoreService, UserCoreService>();
            #endregion

            //other
            services.AddHttpClient();

            //jobs
            //services.AddHostedService<ZomatoJob>();
            //services.AddHostedService<SendMenuJob>();

            //CORS
            services.AddCors();

            //security
            services
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    var serverSecret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:ServerSecret"]));

                    options.TokenValidationParameters = new
                        TokenValidationParameters
                        {
                            IssuerSigningKey = serverSecret,
                            ValidIssuer = Configuration["JWT:Issuer"],
                            ValidAudience = Configuration["JWT:Audience"]
                        };

                });

            //pridani filteru
            services.AddMvc(options => options.Filters.Add(typeof(RsActionFilter)))
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        var logger = context.HttpContext.RequestServices
                            .GetRequiredService<ILogger<Startup>>();
                        logger.LogWarning(3,$"Http Response Information:{Environment.NewLine}" +
                                           $"Schema:{context.HttpContext.Request.Scheme} " +
                                           $"Host: {context.HttpContext.Request.Host} " +
                                           $"Path: {context.HttpContext.Request.Path} " +
                                           $"QueryString: {context.HttpContext.Request.QueryString} " +
                                           $"Response Body: {context.HttpContext.Response.Body}");
                        return new BadRequestObjectResult(context.ModelState);
                    };
                });

            //services.AddSwaggerDocument();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Awesome API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //SWAGGER
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
            //ROUTING
            app.UseRouting();

            //CORS
            //app.UseCors(config => config.WithOrigins(Configuration["JWT:Issuer"]));
            app.UseCors(builder => builder
                .AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());


            //request
            app.UseRequestResponseLogging();

            
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
