﻿using System;
using Infrastructure.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using NHibernate;
using NHibernate.Context;

namespace WebApi.Filters
{
    public class RsActionFilter : IActionFilter
    {
        private readonly ILogger _logger;

        public RsActionFilter(ILogger<RsActionFilter> logger)
        {
            _logger = logger;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            _logger.LogInformation(2,$"Http Response Information:{Environment.NewLine}" +
                              $"Schema:{context.HttpContext.Request.Scheme} " +
                              $"Host: {context.HttpContext.Request.Host} " +
                              $"Path: {context.HttpContext.Request.Path} " +
                              $"QueryString: {context.HttpContext.Request.QueryString} " +
                              $"Response Body: {context.HttpContext.Response.Body}");
            //ISession session = CurrentSessionContext.Unbind(NHibernateHelper._factory);
            //session.Flush();
            //session.Close();
            //session.Dispose();
        }
        

        public void OnActionExecuting(ActionExecutingContext context)
        {
            //CurrentSessionContext.Bind(NHibernateHelper._factory.OpenSession());
        }
    }
}
