﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using ApplicationCore.Models.Mappings;
using ApplicationCore.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebApi.Jobs
{
    public class ZomatoJob : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;
        private readonly JsonSerializer _serializer;
        private readonly IJobCoreServices _jobCoreServices;

        public ZomatoJob(ILogger<ZomatoJob> logger,  IConfiguration configuration, 
            IHttpClientFactory httpClient, IJobCoreServices jobCoreServices)
        {
            _logger = logger;
            _httpClient = httpClient.CreateClient();
            _configuration = configuration;
            _jobCoreServices = jobCoreServices;
            _serializer = new JsonSerializer();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var zomatoSection = _configuration.GetSection("zomato");
            var uri = $"https://developers.zomato.com/api/v2.1/reviews?res_id={zomatoSection["zomatoRestaurantId"]}";
            int days = Convert.ToInt32(zomatoSection["zomatoDay"]);

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.Log(LogLevel.Information, 2, "Ordinary zomato job");

                try
                {
                    _httpClient
                        .DefaultRequestHeaders
                        .Add("user-key", zomatoSection["zomatoUserKey"]);
                    var response =
                        await _httpClient.GetAsync(uri);

                    if (response.IsSuccessStatusCode)
                    {
                        using var sr = new StreamReader(await response.Content.ReadAsStreamAsync());
                        using var jsonTextReader = new JsonTextReader(sr);
                        var zomatoReviews = _serializer.Deserialize<ZomatoReview>(jsonTextReader);

                        if (zomatoReviews.reviews_count > 0)
                        {
                            _jobCoreServices.UpdateZomatoReviews(zomatoReviews);
                        }
                    }
                    else
                    {
                        _logger.Log(LogLevel.Error, 4, "faild communication with Zomato");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Log(LogLevel.Critical, 5, ex.StackTrace);
                }

                await Task.Delay(1000*60*24* days, stoppingToken);
            }
        }
    }
}
