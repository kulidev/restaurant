﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using ApplicationCore.Exceptions;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using Infrastructure.Data.Api;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WebApi.Jobs
{
    public class SendMenuJob : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;
        private readonly INewsletterDao _newsletterDao;
        private readonly IJobCoreServices _jobCoreServices;
        private readonly IEmailService _emailService;

        public SendMenuJob(ILogger logger, IConfiguration configuration, INewsletterDao newsletterDao, IJobCoreServices jobCoreServices, IEmailService emailService)
        {
            _logger = logger;
            _configuration = configuration;
            _newsletterDao = newsletterDao;
            _jobCoreServices = jobCoreServices;
            _emailService = emailService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {

                try
                {
                    var emails = _newsletterDao.GetActiveEmails();

                    if (!emails.IsNullOrEmpty())
                    {
                        var menuMessage = _jobCoreServices.GetFormattedDailyMenu();


                        foreach (var email in emails)
                        {
                            _emailService.SendEmail(menuMessage, email.Email);
                        }
                    }
                }

                catch (MenuException e)
                {
                    _logger.Log(LogLevel.Error, 5, e.Message);
                }

                catch (Exception e)
                {

                    _logger.Log(LogLevel.Critical, 5, e.StackTrace);
                }
            }

#warning vymyslet logiku casovani
            await Task.Delay(60000, stoppingToken);
        }
    }
}
