﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    public class RsBaseController : ControllerBase
    {
        protected readonly ILogger _logger;
        public RsBaseController(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger("My logger");
            //var service = (ILogger)serviceProvider.GetService(typeof(ILogger));
        }
    }
}
