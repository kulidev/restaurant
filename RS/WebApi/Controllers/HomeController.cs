﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : RsBaseController
    {
        private readonly ICustomerCoreService _customerCoreService;

        public HomeController(ILoggerFactory loggerFactory, 
            ICustomerCoreService customerCoreService) 
            : base(loggerFactory)
        {
            _customerCoreService = customerCoreService;
        }

        #region action methods...

        [HttpPost]
        [Route("newsletter")]
        public IActionResult AddEmail(NewsletterRequest request)
        {
            var response = _customerCoreService.AddEmailToNewslleter(request.Email);

            return Ok(response);
        }

        [HttpPost]
        public IActionResult AddReservation(ReservationRequest request)
        {
            var response = _customerCoreService.AddReservation(request);

            return Ok(response);
        }

        [HttpGet, Route("")]
        //[ResponseCache(Duration = 900)]
        public IActionResult GetHomeData()
        {
            HomeResponse response;

            try
            {
                response = _customerCoreService.GetHomeData();
            }
            catch (System.Exception ex)
            {
                _logger.Log(LogLevel.Critical, 5, ex.StackTrace);

#warning delat standardizovane chyby nejlepe podle nejakeho rpc
                var result = new ObjectResult(new { statusCode = 500, message = "Chyba na strane serveru" });
                result.StatusCode = 500;
                return result;
            }

            return Ok(response);
        }

        [HttpGet, Route("other")]
        public IActionResult GetFollowingDailyMenu()
        {
            var response = _customerCoreService.GetDailyMenuResponse();

            if (response == null)
            {
                return BadRequest();
            }

            return Ok(response);
        }



        #endregion
    }
}