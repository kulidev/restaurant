﻿using System;
using System.IO;
using System.Linq;
using ApplicationCore.Interfaces;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Microsoft.AspNetCore.Mvc;
using NHibernate;
using NHibernate.Cfg;

#warning smazat
namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    //[ApiController]
    public class AllergenController : ControllerBase
    {
        private readonly IJobCoreServices _jobCoreServices;
        private readonly IRoleDao _roleDao;
        private readonly IReviewDao _reviewDao;
        private readonly INewsletterDao _newsletterDao;
        private readonly ITableDao _tableDao;
        private readonly IReservationDao _reservationDao;
        private readonly IDrinkDao _drinkDao;
        private readonly IPersonDao _personDao;
        private readonly IOrderMealDao _orderMealDao;

        private readonly ICustomerDao _customerDao;
        private readonly IEmployeeDao _employeeDao;

        private readonly IMenuDao _menuDao;

        private readonly IMealDao _mealDao;

        private readonly IOrderDao _orderDao;

        private readonly IRestaurantDao _restaurantDao;
        private readonly IPersonRoleDao _personRoleDao;

        public AllergenController(IJobCoreServices jobCoreServices, IRoleDao roleDao, IReviewDao reviewDao, INewsletterDao newsletterDao, ITableDao tableDao, IReservationDao reservationDao, IDrinkDao drinkDao, ICustomerDao customerDao, IEmployeeDao employeeDao, IMenuDao menuDao, IMealDao mealDao, IOrderDao orderDao, IRestaurantDao restaurantDao, IPersonRoleDao personRoleDao, IPersonDao personDao, IOrderMealDao orderMealDao)
        {
            _jobCoreServices = jobCoreServices;
            _roleDao = roleDao;
            _reviewDao = reviewDao;
            _newsletterDao = newsletterDao;
            _tableDao = tableDao;
            _reservationDao = reservationDao;
            _drinkDao = drinkDao;
            _customerDao = customerDao;
            _employeeDao = employeeDao;
            _menuDao = menuDao;
            _mealDao = mealDao;
            _orderDao = orderDao;
            _restaurantDao = restaurantDao;
            _personRoleDao = personRoleDao;
            _personDao = personDao;
            _orderMealDao = orderMealDao;
        }

        [HttpGet("")]
        public IActionResult GetThemAll()
        {




            var drink = _drinkDao.LoadById(11);

            //_drinkDao.Delete(drink);


            //BdoDrink drink = new BdoDrink
            //{
            //    Active = true,
            //    DrinkCode = "wine",
            //    Name = "rulanske"

            //};

            //_drinkDao.Save(drink);

           

            return Ok(drink);

            
        }
    }
}