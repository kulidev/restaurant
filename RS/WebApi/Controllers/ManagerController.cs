﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ManagerController : RsBaseController
    {
        private readonly IManagerCoreService _managerCoreService;

        public ManagerController(ILoggerFactory loggerFactory, IManagerCoreService managerCoreService) : base(loggerFactory)
        {
            _managerCoreService = managerCoreService;
        }

        [HttpPost, Route("menu")]
        public IActionResult CreateMenu(string email, MenuRequest request)
        {
            _managerCoreService.CreateMenu(request);

            return NoContent();
        }

        [HttpPut, Route("menu")]
        public IActionResult UpdateMenu(string email, MenuRequest request)
        {
            _managerCoreService.UpdateMenu(request);

            return NoContent();
        }

        [HttpGet, Route("reservation")]
        public IActionResult GetReservation(string email)
        {
            var res = _managerCoreService.GeTableReservation();

            return Ok(res);
        }

        [HttpGet, Route("orders")]
        public IActionResult GetOrders(string email)
        {
            var res = _managerCoreService.GetOrders();

            return Ok(res);
        }

        [HttpGet, Route("reviews/{active}")]
        public IActionResult GetAllReviews(bool? active)
        {

            var response = _managerCoreService.GetReviews(active);

            return Ok(response);
        }

        [HttpPut, Route("reviews/id")]
        public IActionResult UpdateReviews(Review review)
        {
            _managerCoreService.UpdateReview(review);

            return Ok();
        }

        [HttpGet, Route("meals")]
        public IActionResult Get(string email)
        {
            var response = _managerCoreService.GetMeals();

            return Ok(response);
        }

        [HttpPost, Route("meals")]
        public IActionResult CreateMeal(Meal meal)
        {
            _managerCoreService.UpdateMeal(meal);

            return Ok();
        }

        [HttpDelete, Route("meals/id")]
        public IActionResult CreateMeal(int id)
        {
            _managerCoreService.DeleteMeal(id);

            return Ok();
        }

        [HttpPut, Route("meals")]
        public IActionResult UpdateMeal(Meal meal)
        {
            _managerCoreService.UpdateMeal(meal);

            return Ok();
        }

        //[HttpGet]
        //[Route("reservation/{date}")]
        //public IActionResult GetReservation(DateTime? date)
        //{
        //    var response = _managerCoreService.GetReservation(date);

        //    return Ok(response);
        //}

        [HttpDelete, Route("reservation/id")]
        public IActionResult DeleteReservation(int id)
        {
            try
            {
                _managerCoreService.DeleteReservation(id);
            }
            catch (Exception ex)
            {
                _logger.Log(LogLevel.Error, 4, ex.StackTrace);
#warning delat standardizovane chyby nejlepe podle nejakeho rpc
                var result = new ObjectResult(new { statusCode = 500, message = "Chyba na strane serveru" });
                result.StatusCode = 500;
                return result;
            }

            return NoContent();
        }





    }
}