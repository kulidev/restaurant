﻿using System.Linq;
using ApplicationCore.Enums;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class OrderController : RsBaseController
    {
        private readonly ICustomerCoreService _customerCoreService;
        private readonly ICustomerDao _customerDao;
        private readonly IPersonDao _personDao;
        
        public OrderController(ILoggerFactory logger, ICustomerCoreService customerCoreService, ICustomerDao customerDao, IPersonDao personDao) : base(logger)
        {
            _customerCoreService = customerCoreService;
            _customerDao = customerDao;
            _personDao = personDao;
        }

        [HttpGet]
        public IActionResult GetOrders(int customerId, string email)
        {
            var customer = _personDao.LoadById(customerId);

            if (customer == null)
            {
                return BadRequest();
            }

            if (customer.Email != email || !customer.Roles.Any(x=>x.Code == RoleCode.Customer))
            {
                return Unauthorized(new ErrorModel(ErrorCode.Unauthorized, ErrorMessage.Unauthorized));
            }

            var response = _customerCoreService.GetOrders((BdoCustomer)customer);

            return Ok(response);
        }

        [HttpPost, HttpPut]
        public IActionResult CreateOrder(int customerId, string email, OrderRequest request)
        {
            var customer = _personDao.LoadById(customerId);

            if (customer == null)
            {
                return BadRequest();
            }

            if (customer.Email != email || !customer.Roles.Any(x => x.Code == RoleCode.Customer))
            {
                return Unauthorized(new ErrorModel(ErrorCode.Unauthorized, ErrorMessage.Unauthorized));
            }

            if (request.OrderId.HasValue)
            {
                
                _customerCoreService.UpdateOrder((BdoCustomer)customer, request);
            }
            else
            {
                _customerCoreService.CreateOrder((BdoCustomer)customer, request);
            }

            return NoContent();
        }

        [HttpDelete("id")]
        public IActionResult DeleteOrder(int customerId, int id)
        {
            var customer = _personDao.LoadById(customerId);

            if (customer == null)
            {
                return BadRequest();
            }

            _customerCoreService.DeleteOrder((BdoCustomer)customer, id);

            return NoContent();
        }
    }
}