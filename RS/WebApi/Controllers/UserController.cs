﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApplicationCore.Enums;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Data.Api;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IPersonDao _personDao;
        private readonly IUserCoreService _userCoreService;


        public UserController(IConfiguration configuration, IPersonDao personDao, IUserCoreService coreService)
        {
            _configuration = configuration;
            _personDao = personDao;
            _userCoreService = coreService;
        }

        [HttpPost, Route("login")]
        public IActionResult GetToken(LoginRequest request)
        {
            var user = _personDao.GetPersonByEmail(request.Email);

            if (user == null)
            {
                return BadRequest(new ErrorModel(ErrorCode.EmailNotFound, ErrorMessage.UserNotFound));
            }

            var response = _userCoreService.LoginUser(request, user);

            if (response == null)
            {
                return Unauthorized(new ErrorModel(ErrorCode.Unauthorized, ErrorMessage.Unauthorized));
            }

            //Response.Headers.Add("expiresIn", "3600");

            return Ok(response);


            //var authorizationHeader = Request.Headers["Authorization"].First();
            //var key = authorizationHeader.Split(' ')[1];
            //var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(key)).Split(':');
            //var serverSecret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:ServerSecret"]));

            //var user = _personDao.GetPersonByEmail(credentials[0]);

            //if (user == null)
            //{
            //    return BadRequest("uzivatel neexistuje");
            //}

            //if (credentials[1] == user.Password)
            //{
            //    var result = new
            //    {
            //        token = GenerateToken(serverSecret)
            //    };

            //    return Ok(result);
            //}
            //return BadRequest();
        }

        [HttpPost, Route("registration")]
        public IActionResult AddUser(AddUserRequest request)
        {
            var response = _userCoreService.RegisterCustomer(request);

            if (response == null)
            {
                return BadRequest(new ErrorModel(ErrorCode.EmailExist, ErrorMessage.EmailExist));
            }

            return Ok(response);
        }
    }
}