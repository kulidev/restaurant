﻿using System;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : RsBaseController
    {

        private readonly ICustomerCoreService _customerCoreService;

        public ReservationController(ILoggerFactory logger, ICustomerCoreService customerCoreService) : base(logger)
        {
            _customerCoreService = customerCoreService;
        }

        

        //[HttpPost]
        //public IActionResult AddReservation(ReservationRequest request)
        //{
        //    var response = _customerCoreService.AddReservation(request);

        //    return Ok(response);
        //}
    }
}