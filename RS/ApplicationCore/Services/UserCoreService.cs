﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Infrastructure.Data.Domain.PairingTables;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ApplicationCore.Services
{
    public class UserCoreService : IUserCoreService
    {
        private readonly IPersonDao _personDao;
        private readonly IRoleDao _roleDao;
        private readonly IConfiguration _configuration;
        private readonly IPersonRoleDao _personRoleDao;

        public UserCoreService(IPersonDao personDao, IRoleDao roleDao, IConfiguration configuration, IPersonRoleDao personRoleDao)
        {
            _personDao = personDao;
            _roleDao = roleDao;
            _configuration = configuration;
            _personRoleDao = personRoleDao;
        }

        public AddUserResponse RegisterCustomer(AddUserRequest request)
        {
            AddUserResponse response = new AddUserResponse();
            var user = _personDao.GetPersonByEmail(request.Email);

            if (user != null)
            {
                return null;
            }

            byte[] passwordHash, passwordSalt;

            CreatePasswordHash(request.Password, out passwordHash, out passwordSalt);

            BdoCustomer customer = new BdoCustomer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                Password = request.Password,
                SpecialOffer = request.SpecialOffer,
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt
            };

            _personDao.Save(customer);

            var customerRole = _roleDao.GetAll().Where(x => x.Code == "customer").ToList().FirstOrDefault();

            BdoPersonRole bdoPersonRole = new BdoPersonRole
            {
                PersonId = customer.Id,
                RoleId = customerRole.Id
            };

            _personRoleDao.Save(bdoPersonRole);

            response.Result = Infrastructure.Data.Constants.Success;
            response.Message = "Registrace proběhla uspěšne, nyní se můžete přihlásit";
            return response;
        }

        public LoginResponse LoginUser(LoginRequest request, BdoPerson user)
        {
            if (!VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }
            var serverSecret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:ServerSecret"]));

            var token = GenerateToken(serverSecret);

            LoginResponse response = new LoginResponse
            {
                IdToken = token,
                Email = request.Email,
                ExpiresIn = 3500,
                Roles = user.Roles.NullToEmpty().Select(x=> x.Code).ToList(),
                IdUser = user.Id
            };

            return response;
        }

        private string GenerateToken(SecurityKey key)
        {
            var now = DateTime.UtcNow;
            var issuer = _configuration["JWT:Issuer"];
            var audience = _configuration["JWT:Audience"];
            var identity = new ClaimsIdentity();
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var handler = new JwtSecurityTokenHandler();
            var token = handler.CreateJwtSecurityToken(issuer, audience, identity,
                now, now.Add(TimeSpan.FromHours(1)), now, signingCredentials);
            var encodedJwt = handler.WriteToken(token);
            return encodedJwt;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash,
            out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
