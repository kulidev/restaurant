﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Enums;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Infrastructure.Data.Domain.PairingTables;
using NHibernate.Criterion;

namespace ApplicationCore.Services
{
    public class CustomerCoreService : ICustomerCoreService
    {
        private readonly INewsletterDao _newsletterDao;
        private readonly IMenuDao _menuDao;
        private readonly IReviewDao _reviewDao;
        private readonly IRestaurantDao _restaurantDao;
        private readonly IReservationDao _reservationDao;
        private readonly IEmailService _emailService;
        private readonly ICustomerDao _customerDao;
        private readonly IMealDao _mealDao;
        private readonly ITableDao _tableDao;
        private readonly IDrinkDao _drinkDao;
        private readonly IOrderDao _orderDao;
        private readonly IOrderMealDao _orderMealDao;


        public CustomerCoreService(INewsletterDao newsletterDao, IMenuDao menuDao, IReviewDao reviewDao, IRestaurantDao restaurantDao, IReservationDao reservationDao, IEmailService emailService, ICustomerDao customerDao, IMealDao mealDao, ITableDao tableDao, IDrinkDao drinkDao, IOrderDao orderDao, IOrderMealDao orderMealDao)
        {
            _newsletterDao = newsletterDao;
            _menuDao = menuDao;
            _reviewDao = reviewDao;
            _restaurantDao = restaurantDao;
            _reservationDao = reservationDao;
            _emailService = emailService;
            _customerDao = customerDao;
            _mealDao = mealDao;
            _tableDao = tableDao;
            _drinkDao = drinkDao;
            _orderDao = orderDao;
            _orderMealDao = orderMealDao;
        }

        public NewsletterResponse AddEmailToNewslleter(string emailToAdd)
        {
            NewsletterResponse response = new NewsletterResponse();

            var email = _newsletterDao.GetByEmail(emailToAdd);

            if (email == null)
            {
                var newEmail = new BdoNewsletter
                {
                    Active = true,
                    Email = emailToAdd
                };

                _newsletterDao.Save(newEmail);

                response.Message = "Email úspěšně přidán do newsletteru";
            }

            else
            {
                if (email.Active == false)
                {
                    email.Active = true;
                    _newsletterDao.Save(email);

                    response.Message = "Email aktivován v newsletteru";

                }

                response.Message = "Newsletter již obsahuje aktivní email";

            }

            return response;
        }

        public HomeResponse GetHomeData()
        {
            HomeResponse response = new HomeResponse();

            var dailyMenu = _menuDao.GetMenuByDate(DateTime.Now.Date);

            if (dailyMenu != null && !dailyMenu.Meals.IsNullOrEmpty())
            {
                response.DailyMeals = new List<Meal>();

                foreach (var meal in dailyMenu.Meals)
                {
                    response.DailyMeals.Add(TransformMeal(meal));
                }
            }

            var standartMenu = _menuDao.GetStandartMenu();

            if (standartMenu != null && !standartMenu.Meals.IsNullOrEmpty())
            {
                response.StandartMeals = new List<Meal>();

                foreach (var meal in standartMenu.Meals)
                {
                    response.StandartMeals.Add(TransformMeal(meal));
                }

            }

            var drinks = _drinkDao.GetAll();

            if (!drinks.IsNullOrEmpty())
            {
                response.Drinks = new List<Drink>();

                foreach (var d in drinks)
                {
                    response.Drinks.Add(new Drink
                    {
                        Name = d.Name,
                        Portion = d.Portion,
                        Price = d.Price,
                        DrinkCode = d.DrinkCode
                    });
                }
            }

            var reviews = _reviewDao.GetBestReviews(3);

            if (!reviews.IsNullOrEmpty())
            {
                response.Reviews = new List<Review>();
                foreach (var r in reviews)
                {
                    response.Reviews.Add(new Review
                    {
                        Source = r.Source,
                        Rating = r.Rating,
                        ReviewText = r.Review
                    });
                }
            }

            var info = _restaurantDao.GetAll().FirstOrDefault();

            if (info != null)
            {
                response.Info = new Info
                {
                    Email = info.Email,
                    Address = info.Address,
                    OpenHours = info.Address,
                    Phone = info.Phone
                };
            }

            return response;
        }


        /// <summary>
        /// try to create reservationRequest on concrete table
        /// </summary> 
        /// <param name="reservationRequest"></param>
        /// <returns></returns>
        public AddReservationResponse AddReservation(ReservationRequest reservationRequest)
        {
            AddReservationResponse response = new AddReservationResponse();
            int reservationDate = Helper.Helper.
                TransformDateToInt(reservationRequest.Date.Date.ToString("yyyy-MM-dd"));

            var reservations = _reservationDao.GetReservation(reservationDate)
                .NullToEmpty()
                .Where(x => x.StartTime >= reservationRequest.StartTime 
                            || x.StartTime <= reservationRequest.FinishTime);

            var fullTables = reservations.NullToEmpty().Select(x => x.Table.Id).ToList();

            var freeTables = _tableDao.GetAll()
                .Where(x => !fullTables.NullToEmpty().Contains(x.Id)).ToList();

            if (freeTables.Any() 
                && freeTables.Sum(x=>x.Capacity) > reservationRequest.CountPeople)
            {
                if (freeTables.Max(x=>x.Capacity) > reservationRequest.CountPeople)
                {
                    BdoTable table;
                    int i = 0;
                    do
                    {
                        table = 
                            freeTables
                                .FirstOrDefault(x => x.Capacity == reservationRequest.CountPeople + i);
                        i++;
                    } while (table == null);

                    _reservationDao.Save(new BdoReservation
                    {
                        Date = reservationDate,
                        CountPeople = reservationRequest.CountPeople,
                        Email = reservationRequest.Email,
                        OnName = reservationRequest.OnName,
                        FinishTime = reservationRequest.FinishTime,
                        StartTime = reservationRequest.StartTime,
                        Table = table,
                        TookPlace = true
                    });
                    var startTime = $"{reservationRequest.StartTime / 100} : " +
                                    $"{(reservationRequest.StartTime % 100).ToString("00")}  ";
                    _emailService.SendEmail(
                        $"Rezervace na jméno {reservationRequest.OnName} " +
                        $"proběhla úspěšně, budeme se na Vás těšit " +
                        $"{reservationRequest.Date.Date.ToShortDateString()} v {startTime}.",
                        reservationRequest.Email);
                    response.Result = "success";
                    return response;
                }
                response.Result = "needCall";
            }

            else
            {
                response.Result = "fail";
            }
            return response;
        }

        

        public OrderResponse GetOrders(BdoCustomer customer)
        {
            OrderResponse response = new OrderResponse();
            var order = customer.Orders.NullToEmpty().LastOrDefault();

            if (order == null)
            {
                return response;
            }

            response.Meals = new List<Meal>();
            response.Takeaway = order.Takeaway;
            response.OrderId = order.Id;

            foreach (var m in order.Meals)
            {
                response.Meals.Add(
                    new Meal
                    {
                        Allergens = m.Allergens,
                        Name = m.Name,
                        MealCode = m.MealCode,
                        Portion = m.Portion,
                        Price = m.Price,
                        Calories = m.Calories,
                        MeailId = m.Id,
                        Description = m.Description,
                        Image = m.Image
                    });
            }

            return response;
        }

        public void CreateOrder(BdoCustomer customer, OrderRequest request)
        {
            BdoOrder newOrder = new BdoOrder
            {
                Takeaway = request.Takeaway,
                Success = true,
                Date = DateTime.Now.Date,
                Customers = customer
                
            };

            _orderDao.Save(newOrder);

            foreach (var mealId in request.MealIds)
            {
                _orderMealDao.Save(new BdoOrderMeal
                {
                    OrderId = newOrder.Id,
                    MealId = mealId
                });
            }
        }

        public DailyMenuResponse GetDailyMenuResponse()
        {
            var tomorow = DateTime.Now.AddDays(1);
            var finish = DateTime.Now.AddDays(7);

            var menus = _menuDao.GetDailyMenus(tomorow, finish);

            if (!menus.IsNullOrEmpty())
            {
                DailyMenuResponse response = new DailyMenuResponse
                {
                    DailyMenus = new List<DailyMenu>()
                };

                foreach (var menu in menus.NullToEmpty().OrderBy(x=>x.Date))
                {
                    DailyMenu dailyMenu = new DailyMenu
                    {
                        Date = menu.Date,
                        Meals = new List<Meal>(),
                        IdDailyMenu = menu.Id
                    };

                    foreach (var meal in menu.Meals)
                    {
                      dailyMenu.Meals.Add(TransformMeal(meal));  
                    }

                    response.DailyMenus.Add(dailyMenu);
                }

                return response;
            }

            return null;
        }

        public void UpdateOrder(BdoCustomer customer, OrderRequest request)
        {
            var res =_orderMealDao.GetAllByOrderId(request.OrderId.Value);

            foreach (var id in request.MealIds)
            {
                var item = res.FirstOrDefault(x => x.MealId == id);

                if (item == null)
                {
                    _orderMealDao.Save(new BdoOrderMeal
                    {
                        MealId = id,
                        OrderId = request.OrderId.Value
                    });
                }

                else
                {
                    res.Remove(item);
                    
                }
            }

            foreach (var bdoOrderMeal in res)
            {
                _orderMealDao.Delete(bdoOrderMeal);
            }

            var order = _orderDao.LoadById(request.OrderId.Value);

            order.Takeaway = request.Takeaway;

            _orderDao.Update(order);
        }

        public void DeleteOrder(BdoCustomer customer, int orderId)
        {
            var order = _orderDao.LoadById(orderId);

            if (order == null)
            {
                return;
            }

            //var orderMeals = _orderMealDao.GetAllByOrderId(orderId);

            //foreach (var bdoOrderMeal in orderMeals)
            //{
            //    _orderMealDao.Delete(bdoOrderMeal);
            //}

            _orderDao.Delete(order);
        }

        private Meal TransformMeal(BdoMeal meal)
        {
            return new Meal
            {
                Name = meal.Name,
                Allergens = meal.Allergens,
                MealCode = meal.MealCode,
                Portion = meal.Portion,
                Price = meal.Price,
                Calories = meal.Calories,
                Description = meal.Description,
                MeailId = meal.Id,
                Image = meal.Image
            };
        }
    }
}
