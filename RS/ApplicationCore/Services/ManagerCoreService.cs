﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using Infrastructure.Data;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Infrastructure.Data.Domain.PairingTables;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ApplicationCore.Services
{
    public class ManagerCoreService : IManagerCoreService
    {
        private readonly IReviewDao _reviewDao;
        private readonly IMealDao _mealDao;
        private readonly IReservationDao _reservationDao;
        private readonly IPersonDao _personDao;
        private readonly IRoleDao _roleDao;
        private readonly IMenuDao _menuDao;
        private readonly IMealMenuDao _mealMenuDao;
        private readonly ITableDao _tableDao;
        private readonly IOrderDao _orderDao;
        private readonly IOrderMealDao _orderMealDao;

        public ManagerCoreService(IReviewDao reviewDao, IMealDao mealDao, IReservationDao reservationDao, IPersonDao personDao, IRoleDao roleDao, IMenuDao menuDao, IMealMenuDao mealMenuDao, ITableDao tableDao, IOrderDao orderDao, IOrderMealDao orderMealDao)
        {
            _reviewDao = reviewDao;
            _mealDao = mealDao;
            _reservationDao = reservationDao;
            _personDao = personDao;
            _roleDao = roleDao;
            _menuDao = menuDao;
            _mealMenuDao = mealMenuDao;
            _tableDao = tableDao;
            _orderDao = orderDao;
            _orderMealDao = orderMealDao;
        }


        public List<Review> GetReviews(bool? active)
        {
            IEnumerable<BdoReview> reviews;
            if (active.HasValue)
            {
                reviews = _reviewDao.GetReviewsByActive(active.GetValueOrDefault());
            }

            else
            {
                reviews = _reviewDao.GetAll();
            }

            List<Review> response = new List<Review>();
            foreach (var r in reviews)
            {
                response.Add(new Review
                {
                    Source = r.Source,
                    Active = r.Active,
                    Rating = r.Rating,
                    ReviewText = r.Review,
                    ReviewId = r.Id
                });
            }

            return response;
        }

        public void UpdateReview(Review review)
        {
            var bdoReview = _reviewDao.LoadById(review.ReviewId);

            bdoReview.Active = review.Active;

            _reviewDao.Update(bdoReview);
        }

        public List<Meal> GetMeals()
        {
            List<Meal> meals = new List<Meal>();

            var bdoMeals = _mealDao.GetAll();

            foreach (var m in bdoMeals)
            {
                meals.Add(new Meal
                {
                    Allergens = m.Allergens,
                    Name = m.Name,
                    Calories = m.Calories,
                    MeailId = m.Id,
                    Portion = m.Portion,
                    Price = m.Price,
                    MealCode = m.MealCode
                });
            }

            return meals;
        }

        public void CreateMeal(Meal meal)
        {
            _mealDao.Save(new BdoMeal
            {
                Price = meal.Price,
                Active = meal.Active.GetValueOrDefault(false),
                Allergens = meal.Allergens,
                Calories = meal.Calories,
                Name = meal.Name,
                Portion = meal.Portion,
                MealCode = meal.MealCode
            });
        }

        public void UpdateMeal(Meal meal)
        {
            var bdoMeal = _mealDao.LoadById(meal.MeailId);

            if (!meal.Name.IsNullOrEmpty())
            {
                bdoMeal.Name = meal.Name;
            }

            if (!meal.Allergens.IsNullOrEmpty())
            {
                bdoMeal.Allergens = meal.Allergens;
            }

            if (meal.Calories > 0)
            {
                bdoMeal.Calories = meal.Calories;
            }

            if (meal.Active.HasValue)
            {
                bdoMeal.Active = meal.Active.GetValueOrDefault();
            }

            if (!meal.Portion.IsNullOrEmpty())
            {
                bdoMeal.Portion = meal.Portion;
            }

            if (meal.Price > 0)
            {
                bdoMeal.Price = meal.Price;
            }

            if (!meal.MealCode.IsNullOrEmpty())
            {
                bdoMeal.MealCode = meal.MealCode;
            }

            _mealDao.Update(bdoMeal);
        }

        public void DeleteMeal(int id)
        {
            //var meal = _mealDao.LoadById(id);

            //if (meal != null)
            //{
            //    _mealDao.Delete(meal);
            //}
        }

        public ReservationResponse GetReservation(DateTime? date = null)
        {
            if (!date.HasValue)
            {
                date = DateTime.Now.Date;
            }

            var reservations = _reservationDao.GetReservation(Helper.Helper.TransformDateToInt(date.Value.ToString("yyyy-MM-dd")));

            var response = new ReservationResponse
            {
                Reservations = new List<ReservationRequest>()
            };
            foreach (var r in reservations)
            {
                response.Reservations.Add(new ReservationRequest()
                {
                    //Date = r.Date.Date,
                    CountPeople = r.CountPeople,
                    OnName = r.OnName,
                    StartTime = r.StartTime,
                    FinishTime = r.FinishTime,
                    Email = r.Email
                });
            }

            return response;

        }

        public void DeleteReservation(int id)
        {
            //var reservation = _reservationDao.LoadById(id);

            //_reservationDao.Delete(reservation);
        }

        public void CreateMenu(MenuRequest request)
        {
            BdoMenu menu = new BdoMenu();
            menu.MenuCode = Constants.MenuTypeDaily;
            menu.Date = request.Date;

            _menuDao.Save(menu);

            foreach (var mealId in request.MealsId)
            {
                _mealMenuDao.Save(new BdoMealMenu
                {
                    MealId = mealId,
                    MenuId = menu.Id
                });
            }
        }

        public void UpdateMenu(MenuRequest request)
        {
            throw new NotImplementedException();
        }

        public TableReservationResponse GeTableReservation()
        {
            var response = new TableReservationResponse
            {
                ReservationsReservationRequests = new List<ReservationRequest>()
            };

            var tables = _tableDao.GetAll();

            foreach (var table in tables)
            {
                foreach (var reservation in table.Tables)
                {
                    response.ReservationsReservationRequests.Add(new ReservationRequest
                    {
                        Table = table.Id,
                        Capacity = table.Capacity,
                        DateInt = reservation.Date,
                        Email = reservation.Email,
                        StartTime = reservation.StartTime,
                        FinishTime = reservation.FinishTime,
                        CountPeople = reservation.CountPeople,
                        OnName = reservation.OnName
                    });
                }
                
            }

            return response;
        }

        public GetOrderResponse GetOrders()
        {
            var orders = _orderDao.GetAll();

            var response = new GetOrderResponse
            {
                Orders = new List<GetOrder>()
            };

            foreach (var o in orders)
            {
                foreach (var m in o.Meals)
                {
                    response.Orders.Add(new GetOrder
                    {
                        Meal = new Meal
                        {
                            Name = m.Name,
                            Price = m.Price
                        },
                        OrderId = o.Id,
                        Takeaway = o.Takeaway,
                        Success = o.Success,
                        CustomerId = o.Customers.Id,
                        CustomerName = $"{o.Customers.FirstName} {o.Customers.FirstName}"
                    });
                }
            }

            return response;

        }

        
    }
}