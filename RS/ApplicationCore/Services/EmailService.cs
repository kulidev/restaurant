﻿using System;
using System.Net;
using System.Net.Mail;
using ApplicationCore.Interfaces;
using Microsoft.Extensions.Configuration;

namespace ApplicationCore.Services
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration _configuration;

        public EmailService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public void SendEmail(string message, string emailTo)
        {
            var mailSection = _configuration.GetSection("email");

            // Credentials
            var credentials = new NetworkCredential(mailSection["username"], mailSection["password"]);

            // Mail message
            var mail = new MailMessage()
            {
                From = new MailAddress(mailSection["mailAdress"]),
                Subject = $"denní menu {DateTime.Now.ToShortDateString()}",
                Body = message
            };
            mail.IsBodyHtml = true;
            mail.To.Add(new MailAddress(emailTo));

            // Smtp client
            var client = new SmtpClient()
            {
                Port = 587,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Host = mailSection["host"],
                EnableSsl = true,
                Credentials = credentials
            };
            client.Send(mail);
        }
    }
}