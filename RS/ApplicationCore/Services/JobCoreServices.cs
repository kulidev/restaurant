﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ApplicationCore.Exceptions;
using ApplicationCore.Extensions;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using ApplicationCore.Models.Mappings;
using Infrastructure.Data;
using Infrastructure.Data.Api;
using Infrastructure.Data.Domain;
using Microsoft.Extensions.Configuration;

namespace ApplicationCore.Services
{
    public class JobCoreServices : IJobCoreServices
    {
        private readonly IReviewDao _reviewDao;
        private readonly IMenuDao _menuDao;
        private readonly IConfiguration _configuration;


        public JobCoreServices(IReviewDao reviewDao, IMenuDao menuDao, IConfiguration configuration)
        {
            _reviewDao = reviewDao;
            _menuDao = menuDao;
            _configuration = configuration;
        }

        public void UpdateZomatoReviews(ZomatoReview reviews)
        {
            var lastReviewId = _reviewDao.GetLast();

            List<BdoReview> reviewsToSave = new List<BdoReview>();

            var review = reviews.user_reviews.FirstOrDefault(x => x.review.id == lastReviewId);

            if (review != null)
            {
                int positionToSlice = reviews.user_reviews.IndexOf(review);
                reviews.user_reviews = reviews.user_reviews.Skip(positionToSlice + 1).ToList();
            }
            
            foreach (var r in reviews.user_reviews)
            {
                if (lastReviewId.HasValue && r.review.id == lastReviewId)
                {
                    break;
                }

                reviewsToSave.Add(new BdoReview
                {
                    IdSource = r.review.id,
                    Rating = r.review.rating,
                    Review = r.review.rating_text,
                    Source = Constants.Zomato,
                    Active = false
                });
            }

            if (reviews.user_reviews.Count > 0)
            {
                _reviewDao.SaveCollection(reviewsToSave); 
            }
        }

        public string GetFormattedDailyMenu()
        {
            var menu = _menuDao.GetMenuByDate(DateTime.Now);

            if (menu == null || menu.Meals.IsNullOrEmpty())
            {
                throw new MenuException($"neni pripravene daily menu pro {DateTime.Now}");
            }

            StringBuilder sb = new StringBuilder();

            foreach (var meal in menu.Meals)
            {
                sb.Append(meal.Portion + " " + meal.Name + " " + meal.Allergens + "   " + meal.Price + Environment.NewLine);
            }

            var restaurantName = _configuration.GetValue<string>("restaurantName");
            sb.Append(Environment.NewLine + restaurantName);

            return sb.ToString();
        }
    }
}
