﻿namespace ApplicationCore.Enums
{
    public static class RoleCode
    {
        public const string Customer = "customer";
        public const string Admin = "admin";
        public const string Employee = "employee";
        public const string Manager = "manager";
    }

    public static class ErrorCode
    {
        public const string EmailExist = "EMAIL_EXISTS";
        public const string EmailNotFound = "EMAIL_NOT_FOUND";
        public const string InvalidPassword = "INVALID_PASSWORD";

        public const string Unauthorized = "UNAUTHORIZED";
        public const string UserNotFound = "USER_NOT_FOUND";
    }

    public static class ErrorMessage
    {
        public const string EmailExist = "Tento email je již zaregistrován";
        public const string Unauthorized = "Přístup zamítnu, obraťte se na správce";
        public const string UserNotFound = "Uživatel nenalezen";
    }
}