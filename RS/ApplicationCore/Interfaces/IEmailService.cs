﻿namespace ApplicationCore.Interfaces
{
    public interface IEmailService
    {
        void SendEmail(string message, string emailTo);
    }
}