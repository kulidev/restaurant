﻿using ApplicationCore.Models;
using Infrastructure.Data.Domain;

namespace ApplicationCore.Interfaces
{
    public interface IUserCoreService
    {
        AddUserResponse RegisterCustomer(AddUserRequest request);

        LoginResponse LoginUser(LoginRequest request, BdoPerson user);
    }
}