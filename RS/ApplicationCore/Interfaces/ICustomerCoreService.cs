﻿using System;
using ApplicationCore.Enums;
using ApplicationCore.Models;
using Infrastructure.Data.Domain;

namespace ApplicationCore.Interfaces
{
    public interface ICustomerCoreService
    {
        NewsletterResponse AddEmailToNewslleter(string emailToAdd);

        HomeResponse GetHomeData();

        AddReservationResponse AddReservation(ReservationRequest reservationRequest);

        OrderResponse GetOrders(BdoCustomer customer);

        void CreateOrder(BdoCustomer customer, OrderRequest request);

        DailyMenuResponse GetDailyMenuResponse();

        void UpdateOrder(BdoCustomer customer, OrderRequest request);

        void DeleteOrder(BdoCustomer customer, int orderId);
    } 
}
