﻿using ApplicationCore.Models;
using ApplicationCore.Models.Mappings;

namespace ApplicationCore.Interfaces
{
    public interface IJobCoreServices
    {
        void UpdateZomatoReviews(ZomatoReview reviews);

        string GetFormattedDailyMenu();
    }
}