﻿using System;
using System.Collections.Generic;
using ApplicationCore.Models;

namespace ApplicationCore.Interfaces
{
    public interface IManagerCoreService
    {
        //Review section

        List<Review> GetReviews(bool? active);

        void UpdateReview(Review review);

        //Meal section

        List<Meal> GetMeals();

        void CreateMeal(Meal meal);

        void UpdateMeal(Meal meal);

        void DeleteMeal(int id);

        ReservationResponse GetReservation(DateTime? date = null);

        void DeleteReservation(int id);

        void CreateMenu(MenuRequest request);

        void UpdateMenu(MenuRequest request);

        TableReservationResponse GeTableReservation();

        GetOrderResponse GetOrders();
    }
}