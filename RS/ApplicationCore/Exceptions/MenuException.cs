﻿using System;

namespace ApplicationCore.Exceptions
{
    public class MenuException : Exception
    {
        public MenuException(string message) : base(message)
        {
            
        }
    }
}