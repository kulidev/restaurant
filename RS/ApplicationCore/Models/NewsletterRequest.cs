﻿using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Models
{
    public class NewsletterRequest
    {
        [Required, EmailAddress]
        public string Email { get; set; }
    }
}

