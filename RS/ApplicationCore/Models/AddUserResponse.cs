﻿namespace ApplicationCore.Models
{
    public class AddUserResponse
    {
        /// <summary>
        /// enum {success, fail}
        /// </summary>
        public string Result { get; set; }

        public string Message { get; set; }
    }
}