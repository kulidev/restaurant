﻿namespace ApplicationCore.Models
{
    public class Drink
    {
        public string Name { get; set; }

        public int Price { get; set; }

        /// <summary>
        /// enum {beer, wine}
        /// </summary>
        public string DrinkCode { get; set; }

        public string Portion { get; set; }
    }
}