﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Models
{
    public class AddReservationResponse
    {
        public string Result { get; set; }
    }
}
