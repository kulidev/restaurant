﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Models
{
    public class ReservationRequest
    {
        [Required]
        public string OnName { get; set; }

        [Required, Range(1, 10)]
        public int CountPeople { get; set; }

        [Required]
        public int StartTime { get; set; }

        [Required]
        public int FinishTime { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public string Email { get; set; }

        public int Table { get; set; }

        public int Capacity { get; set; }

        public int DateInt { get; set; }
    }
}
