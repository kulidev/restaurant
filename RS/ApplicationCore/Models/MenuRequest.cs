﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class MenuRequest
    {
        public List<int> MealsId { get; set; }

        public DateTime Date { get; set; }

        public int? MenuId { get; set; }
    }
}