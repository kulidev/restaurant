﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.Models
{
    public class HomeResponse
    {
        public List<Meal> DailyMeals{ get; set; }

        public List<Meal> StandartMeals { get; set; }

        public List<Drink> Drinks { get; set; }

        public List<Review> Reviews { get; set; }

        public Info Info { get; set; }

        
    }
}
