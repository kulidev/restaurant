﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class ReservationResponse
    {
        public List<ReservationRequest> Reservations { get; set; }
    }
}
