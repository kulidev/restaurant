﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Models
{
    public class GetOrder
    {
        public int OrderId { get; set; }

        public Meal Meal { get; set; }

        public bool Takeaway { get; set; }

        public bool Success { get; set; }

        public string CustomerName { get; set; }

        public int CustomerId { get; set; }
    }
}
