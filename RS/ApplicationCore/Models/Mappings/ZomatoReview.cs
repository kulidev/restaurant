﻿using System.Collections.Generic;

namespace ApplicationCore.Models.Mappings
{
    public class ZomatoReview
    {
        public int reviews_count { get; set; }
        public int reviews_start { get; set; }
        public int reviews_shown { get; set; }
        public List<UserReview> user_reviews { get; set; }
    }

    public class User
    {
        public string name { get; set; }
        public string foodie_level { get; set; }
        public int foodie_level_num { get; set; }
        public string foodie_color { get; set; }
        public string profile_url { get; set; }
        public string profile_image { get; set; }
        public string profile_deeplink { get; set; }
    }

    public class Review
    {
        public int rating { get; set; }
        public string review_text { get; set; }
        public int id { get; set; }
        public string rating_color { get; set; }
        public string review_time_friendly { get; set; }
        public string rating_text { get; set; }
        public int timestamp { get; set; }
        public int likes { get; set; }
        public User user { get; set; }
        public int comments_count { get; set; }
    }

    public class UserReview
    {
        public Review review { get; set; }
    }
}
