﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class OrderRequest
    {
        public List<int> MealIds { get; set; }

        public bool Takeaway { get; set; }

        public int? OrderId { get; set; }
    }
}