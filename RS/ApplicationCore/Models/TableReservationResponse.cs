﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class TableReservationResponse
    {
        public IList<ReservationRequest> ReservationsReservationRequests { get; set; }
    }
}
