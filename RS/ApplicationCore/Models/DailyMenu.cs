﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Models
{
    public class DailyMenu
    {
        public List<Meal> Meals { get; set; }

        public DateTime Date { get; set; }

        public int IdDailyMenu { get; set; }
    }
}
