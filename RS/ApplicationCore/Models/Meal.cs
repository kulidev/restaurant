﻿namespace ApplicationCore.Models
{
    public class Meal
    {
        public int MeailId { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }

        public string Portion { get; set; }

        public int Calories { get; set; }

        public string Allergens { get; set; }

        public string MealCode { get; set; }

        public bool? Active { get; set; }

        public string Description { get; set; }

        public string Image { get; set; }
    }
}
