﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class DailyMenuResponse
    {
        public List<DailyMenu> DailyMenus { get; set; }

        
    }
}