﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class GetOrderResponse
    {
        public List<GetOrder> Orders { get; set; }
    }
}
