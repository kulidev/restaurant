﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public List<Meal> Meals { get; set; }

        public bool Takeaway { get; set; }
    }
}
