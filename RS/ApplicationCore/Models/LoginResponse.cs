﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class LoginResponse
    {
        public string IdToken { get; set; }

        public int ExpiresIn { get; set; }

        public string Email { get; set; }

        public List<string> Roles { get; set; }

        public int IdUser { get; set; }
        
    }
}