﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class OrderResponse
    {
        public int OrderId { get; set; }

        public List<Meal> Meals { get; set; }

        public bool Takeaway { get; set; }
    }
}