﻿namespace ApplicationCore.Models
{
    public class NewsletterResponse
    {
        public string Message { get; set; }
    }
}