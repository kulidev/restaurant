﻿namespace ApplicationCore.Models
{
    public class Review
    {
        public int Rating { get; set; }

        public string ReviewText { get; set; }

        /// <summary>
        /// review company provider (zomato, trip advisor..)
        /// </summary>
        public string Source { get; set; }

        public bool Active { get; set; }

        public int  ReviewId { get; set; }
    }
}