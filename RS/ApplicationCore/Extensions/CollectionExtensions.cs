﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ApplicationCore.Extensions
{
    public static class CollectionExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection)
        {
            return collection == null || !collection.Any();
        }

        public static IEnumerable<T> NullToEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable ?? System.Linq.Enumerable.Empty<T>();
        }

        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            if (items == null)
            {
                return;
            }


            foreach (var item in items)
            {
                action(item);
            }
        }

        public static bool In(this Enum @object, params Enum[] flags)
        {
            foreach (var flag in flags)
                if (Equals(flag, @object)) return true;

            return false;
        }
    }
}
