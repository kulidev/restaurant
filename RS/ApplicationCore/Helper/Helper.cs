﻿namespace ApplicationCore.Helper
{
    public static class Helper
    {
        public static int TransformDateToInt(string date)
        {
            var splitDate = date.Split('-');
            var stringDate = $"{splitDate[0]}{splitDate[1]}{splitDate[2]}";
            return int.Parse(stringDate);
        }
        
    }
}